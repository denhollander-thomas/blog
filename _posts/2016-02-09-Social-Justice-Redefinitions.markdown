---
layout: post
title:  "Social Justice Redefinitions"
date:   2016-02-09
categories: politics
---

Do you have an ideology and want to force it unto others? Do you face backlash from people with 'liberal' ideals? It is perfectly understandable if your ideology isn't perfect, in fact, it's highly likely it has more than a few flaws. But instead of converting to another one or *gasp* becoming a moderate, why not redefine the terms others use to serve you? Here are some example mental gymnastics to help you get started!

# Free speech - denying people a voice based on etnicity

Free speech is the ability to say whatever you want whenever you want. Sadly, many ethnic minorities have been denied a voice for a long time because of the oppression by the ruling class. Because it is important that these people can express themselves a special space should be created where they can speak up without being silenced or attacked with hate speech. Note that privileged people often won't realize they are using oppressive language, since they will already have internalized these problematic power dynamics. For everyone to have a say it is advisable, nay necessary, for the powerful to listen instead of shout to the underprivileged and to assign special areas for this purpose.

# Racism, sexism - only when you do it

Racism and sexism is judging people solely on they race of gender. Racism and sexism are always present in our society even when we might not realize it because we have accepted them as normal. Because of this, the perpetrators always exert institutional power on the victims. Obviously it is impossible to abuse this power when society denies you any. In fact, even when oppressed groups try to discriminate they are still the victims as the failure of their attempt enforces the fact that they are oppressed. Therefore, oppressed groups in society can't possibly be racist or sexist.

# Hate speech - anything someone might not like

According to the Oxford Dictionary, hate speech is speech expressing hatred or intolerance of other social groups, especially on the basis of race or sexuality. Intolerance and hate may come in many forms though. Surely implicit hatred is just as bad if not worse? The problem lies with the fact that people with problematic thoughts do not realize the nature of their ideas. If this extends to an entire society having degrading views to certain groups of people, clearly they should not be able to judge what is hate speech. The only people who can objectively decide whether something is in fact hate speech are the people to whom it's directed.


You see? It's easy! If you enjoyed these, why not try to find some new meanings for terminology you dislike, such as 'innocent until proven guilty' or 'censorship'? Have fun!