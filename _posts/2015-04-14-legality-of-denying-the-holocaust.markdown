---
layout: post
title:  "Legality of denying the holocaust"
date:   2015-04-14
categories: politics
---

Denying the holocaust is a bad thing and most of us will agree on that. [In 16 countries, mainly in Europe, it is illegal](https://en.wikipedia.org/wiki/Laws_against_Holocaust_denial), in some explicitly and in others it is punishable as discrimination. Whether vocal discrimination should be punishable is another question, and although there are many subtleties to it, I think it shouldn't, but I would like to focus on the specific issue of denying genocidal acts.

We are certain the holocaust happened, yet some insist on ignoring the evidence. In doing so they hurt many people that have associations with the second world war. Why should anyone support their clearly bad ideas? Anti-discriminatory laws are easy to defend because people instinctively know discrimination is bad. Defending free speech on such subjects can be and is being distorted into supporting the views themselves. It is an easy straw man that's tempting to use.

A limit on free speech is always foggy. It is impossible to account for jokes and cynicism. Hate speech could be defended by claiming it is satire and similarly jokes can be misinterpreted to be punishable. It is impossible to know someone's intentions without reading their minds, and that thought is even scarier than punishing innocent people.

Additionally these laws can be abused. We have to remember that laws are supposed to work at all times. Ideals are universally applicable. If a totalitarian government wanted to change history and use the law to make it mandatory to accept their version, we would be against it. As stated before, the holocaust has happened, but let's say, theoretically, it hadn't, or evidence suggested a substantially different story, then announcing that would be punishable. The fact that in theory we could be entirely right and still punishable, should frighten anyone.

But it also undermines the evidence. There are excellent and clear reasons to accept the history. Making something lawfully true makes the evidence unnecessary, the word of the state against the word of some people. Evidence is the only way an image of  history can be formed. In other words, when denying some part of history is punishable, it doesn't matter anymore what really happened. The actual events are risked to be, counterproductively, reduced to the law itself. By trying to enforce truth one makes it not about reality anymore. Imagine if for example the Higgs-boson would be included in the law in the same way. Would experimental evidence, even evidence proving it, matter if it was recorded as a definite truth? Even the laws of gravity, evidently and undeniably truth, would be preposterous to define legally. History is not different in essence, yet is treated differently.

The concepts we are most certain of should be questionable to ascertain their validity and if they can't be their validity becomes irrelevant. That's why it is so important not to limit free speech in such a way. As many have said in much better words, a defender of free speech should also defend the right to have ideas they disagree with.