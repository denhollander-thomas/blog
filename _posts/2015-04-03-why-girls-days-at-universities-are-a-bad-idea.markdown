---
layout: post
title:  "Why 'Girls days' at universities are a bad idea"
date:   2015-04-03
categories: politics
---

*Note: this post was ported from my Blogger page. While I still agree with the premise of this post, the reasoning and phrasing could have been more careful.*

Lately, more and more universities have started organizing special 'girls days' (or, conforming to the stereotype that all girls love pink, 'pink days'). They are meant to encourage girls to go into STEM fields, while boys are not allowed to attend. Many people will immediately point out this is a bad idea. They say that trying to reduce inequality with discrimination is a bad idea. In this blog post, I want to consider a couple of situations and find out whether the good outweighs the bad in some or all situations.

# If there are no natural differences between men and women

People agreeing with these specialised days generally seem to be of the opinion that there are no essential differences between men and women. Except for the obvious fysical characteristics, both genders behave and think equally. As Christina Hoff Sommers has pointed out, this leads to a situation where one cannot support transgenders without being a hypocrite. Clearly, no one can be born with the wrong gender, as gender in this case just describes the hull. Since that realisation alone doesn't necessary mean it's wrong (it could be that there's no such thing as being born in the wrong body, but rather it's all cultural), and since this seems a rather popular stance, it is worth considering.

If it is true, and it might be, then that means that any unequal ratio between men and women in universities can only be explained by sexism. It is clear that the issue does not lie within universities, but let's consider it anyway. One might think universities somehow are selecting people based on their gender or discouraging them subconsciously. Not only is this very unlikely but it is also hard to check. In any case the special day method is the wrong way to handle it. The problem should then be addressed from within and rising popularity with women is only fixing the symptoms.

Others say the problem lies with society as a whole. According to them, people want others to conform to stereotypes and women are being discouraged to go into STEM because society wrongly thinks they aren't suitable. It is impossible to give single clear examples, but rather society as a whole is to blame. But no example is needed either, since it happens subcontiously everywhere. No proof can be obtained since a situation can be explained by several theories and sexism in this case is a unfalsifiable hypothesis. Look at the result, they say, it is clear that it happens, and if you disagree, you're part of the problem. The inequality just being there is enough evidence of a structural flaw.

Even if women are invisibly and continuously influenced in this way, orginizing special days would be a bad idea. The balance might tip when women are portrayed differently than men. It is not inconceivably for example that STEM will now be thought of as a girly thing since that's the image they advertise with, or, even more likely, that these campaigns perpetuate the idea that women are somehow weaker and need more help to follow their dreams. Why would women even try if it is so much harder for them? You might think that women are not as easily influenced, but remember that it is not me making that assertion. It might give the idea that women are afraid of being with men and only dare to speak out when alone with other women. Their surroundings will helpfully warn them for the evil that will haunt them when they'll actually be studying at a university. No one will protect them from men there (yet). Lastly, what idea does it give to men? Why are women worth having special days for but any mens' days are (luckily) never even spoken of? Clearly men are worth nothing, they are just there, and women are what universities are actually looking for.

We shouldn't forget either that, if inequality of outcome is entirely the result of sexism, then comparing gender to skin colour would be valid. There are only outside differences. No one even dares bringing up 'black days' to which only people of colour are allowed to attend. Why is gender any different?

# If there are natural differences between men and women

What if there are natural differences between men and women? To answer the question I want to divide the issue into two smaller groups. There might be differences of interest or differences of quality, and there might be a combination of the two, in which case both category's arguments apply. In both cases, it is strange that the problem is only addressed one way. There are no campaigns for men in healthcare, to give an example, while there centainly are gender gaps there.

## Differences in quality

There might be differences in quality. Men, generally, might be better at technical fields, while women, generally, are better in social fields. If this is true, the answer is clear. By following a quota of men and women you are automatically accepting less suitable women and denying more suitable boys access to university. Since there are more suitable men, the ratio of men versus women would be skewed towards men when just looking at their qualities. Even if there is no maximum number of students, women are advertised for more than men and therefore less suitable women are encouraged while more suitable men are neglected.

## Differences in interest

Lastly, there might be a difference in interest between men and women. This might actually be the most likely scenario, and is also the only one in which a fairly good point could be made towards advertising only or especially to women. If there are differences in interest then this would mean men are naturally more drawn towards technical fields. The stereotype of little boys playing with cars and girls playing with dolls might have some reason to exist. Having special days for women is trying to change their natural interest, which doesn't have to be bad. In a sense, all advertising is changing someone's interest and it is the university's right to do as they like. If it means getting more students, why not?

They are, however, still making a mistake. By alienating men they possibly discourage them from going. They may actually be undoing the extra students gained by advertising for women. Additionally, by trying to reach a arbitrary ratio they could well cause people to regret their decision later on, both by encouraging women who aren't interested, and discouraging men who are.

It is clear that in most situations, specialised gender days are a silly idea. We simply can't oversee the results so that, even if it isn't immediately clear they are bad in a certain situation, then at least they could be worse than not having them.
